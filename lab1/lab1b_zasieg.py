import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import random as rnd
import math

delta = .1
x_size = 10
y_size = 10

kp = 2
k0 = k0i = 1
d0 = 3

# zasieg widzenia przeszkod
sight = .5

def rnd_point(a, b):
    return (rnd.randint(a,b), rnd.randint(a,b))

obst_vect = [rnd_point(-10,10), rnd_point(-10,10), rnd_point(-10,10), rnd_point(-10,10), rnd_point(-10,10)]
start_point=(-10,round(rnd.uniform(0.0, 10.0),1))
print "starting at"
print start_point
finish_point=(10,round(rnd.uniform(0.0, 10.0),1))
print "destination"
print finish_point


def distance(a, b):
    return math.sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2)

def Fp(q):
    return kp * distance(q[0],q[1])

def Foi(q):
    if distance(q[0], q[1]) <= d0:
        if distance(q[0], q[1]) == 0:
            return 1000.0
        return -k0*(1/distance(q[0], q[1]) - 1/d0) * (1/(distance(q[0],q[1])**2))
    return 0.0

def allFoi(point):
    Sum = 0
    for (a,b) in obst_vect:
         Sum+=Foi(((point),(a,b)))
    return Sum

@np.vectorize
def calcZ(x,y):
    return Fp(((x, y), finish_point)) - allFoi((x, y))

def angleBetween(v1):
    return 180*(math.atan2(v1[1],v1[0])/math.pi)

def vectorFP(p1,p2):
    return (p2[0]-p1[0], p2[1]-p1[1])

def makeOne(p1):
    d = max(p1[0], p1[1])
    if d == 0:
        return (p1[0], p1[1])
    return (p1[0]/d, p1[1]/d)

def addVector(v1,v2):
    return (v1[0]+v2[0],v1[1]+v2[1])

def dotProdConst(v1,a):
    return (v1[0]*a, v1[1]*a)

def calcV(point):
    sumVF = (0,0)
    for (a,b) in obst_vect:
        obstVector = vectorFP((point), (a,b))
        if np.linalg.norm(obstVector) < sight:
            normalizedObstVector = makeOne(obstVector)
            obstVectorMagnitude = Foi(((point), (a, b)))
            sumVF = addVector(sumVF, dotProdConst(normalizedObstVector, obstVectorMagnitude))
    finishPVector = vectorFP((point), (finish_point))
    normalizedFinishVector = makeOne(finishPVector)
    finishPVectorMagnitude =  Fp(((point), finish_point))
    sumVF = addVector(sumVF, dotProdConst(normalizedFinishVector, finishPVectorMagnitude))
    return sumVF

def reachedFinish(rob_pos):
    if (finish_point[0]-2*delta < rob_pos[0] < finish_point[0]+2*delta) \
            and (finish_point[1]-2*delta < rob_pos[1] < finish_point[1]+2*delta):
        return True
    return False

def updateMap(map, rob_pos):
    map.plot(rob_pos[0], rob_pos[1], "or", color='pink')

def move(start_pos, map):
    rob_pos = start_pos
    new_rob_pos = (0,0)
    it = 0
    while not reachedFinish(rob_pos):
        angle = angleBetween(calcV(rob_pos))
        if angle < 0:
            angle = 360 + angle
        print "angle: ",angle
        if (337.5) <= angle or angle < (22.5):
            new_rob_pos = (rob_pos[0]+delta, rob_pos[1])
        if (-22.5+1*45) <= angle < (22.5+1*45):
            new_rob_pos = (rob_pos[0]+delta, rob_pos[1]+delta)
        if (-22.5 + 2 * 45) <= angle < (22.5 + 2 * 45):
            new_rob_pos = (rob_pos[0], rob_pos[1] + delta)
        if (-22.5 + 3 * 45) <= angle < (22.5 + 3 * 45):
            new_rob_pos = (rob_pos[0] - delta, rob_pos[1] + delta)
        if (-22.5 + 4 * 45) <= angle < (22.5 + 4 * 45):
            new_rob_pos = (rob_pos[0] - delta, rob_pos[1])
        if (-22.5 + 5 * 45) <= angle < (22.5 + 5 * 45):
            new_rob_pos = (rob_pos[0] - delta, rob_pos[1] - delta)
        if (-22.5 + 6 * 45) <= angle < (22.5 + 6 * 45):
            new_rob_pos = (rob_pos[0], rob_pos[1] - delta)
        if (-22.5 + 7 * 45) <= angle < (22.5 + 7 * 45):
            new_rob_pos = (rob_pos[0] + delta, rob_pos[1] - delta)
        rob_pos = (round(new_rob_pos[0],1), round(new_rob_pos[1],1))
        updateMap(map, rob_pos)
        print "rob_pos: ",rob_pos
        it = it +1
        if it>1000:
            print "no. f steps > 1000"
            return False
    print "steps: ", it
    return True




x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = calcZ(X, Y)


fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=60, vmin=np.amin(Z))


plt.plot(start_point[0], start_point[1], "or", color='red')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')

move(start_point, plt)

plt.grid(True)
plt.show()


