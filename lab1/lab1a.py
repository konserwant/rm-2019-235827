import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import random as rnd
import math

delta = .1
x_size = 10
y_size = 10

kp = 2
k0 = k0i = 1
d0 = 20


def rnd_point(a, b):
    return (rnd.randint(a,b), rnd.randint(a,b))

def distance(a, b):
    return math.sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2)

def Fp(q):
    return kp * distance(q[0],q[1])

def Foi(q):
    if distance(q[0], q[1]) <= d0:
        if distance(q[0], q[1]) == 0:
            return 1000.0
        return -k0*(1/distance(q[0], q[1]) - 1/d0) * (1/(distance(q[0],q[1])**2))
    return 0.0

def allFoi(point):
    Sum = 0
    for (a,b) in obst_vect:
         Sum+=Foi(((point),(a,b)))
    return Sum

@np.vectorize
def calcZ(x,y):
    return Fp(((x, y), finish_point)) - allFoi((x, y))


obst_vect = [rnd_point(-10,10), rnd_point(-10,10), rnd_point(-10,10), rnd_point(-10,10)]
start_point=(-10,rnd.randint(0,10))
finish_point=(10,rnd.randint(0,10))


x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = calcZ(X, Y)

print np.amax(Z)

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=60, vmin=np.amin(Z))


plt.plot(start_point[0], start_point[1], "or", color='red')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
